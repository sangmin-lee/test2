package com.example.testtutorial;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberSignUpService {

  private final MemberRepository repository;

  public Member signUp(SignUpRequest dto) {
    if (repository.existsByEmail(dto.getEmail())) {
      throw new EmailAlreadyExistsExceptions();
    }
    Member member = new Member(dto.getEmail(), dto.getName());
    return repository.save(member);
  }


}
