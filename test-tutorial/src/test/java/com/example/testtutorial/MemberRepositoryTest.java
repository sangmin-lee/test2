package com.example.testtutorial;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
public class MemberRepositoryTest {

  @Autowired
  private MemberRepository memberRepository;

  private Member saveMember;
  private String email;

  @BeforeEach
  void setUp() throws Exception {
    email = "asd@asd.com";
    final Name name = Name.builder()
        .first("a").middle("b").last("c").build();
    saveMember = memberRepository.save(new Member(email, name));
  }

  @Test
  @DisplayName("이메일이 존재하는 경우")
  void givenEmailWhenCallExistsByEmailThenReturnTrue() {
    // when
    final boolean existsByEmail = memberRepository.existsByEmail(email);

    // then
    assertThat(existsByEmail).isTrue();
  }

  @Test
  @DisplayName("이메일이 존재하지 않는 경우")
  void givenEmailWhenWhenCallExistsByEmailThenReturnFalse() {
    // given
    final String newEmail = "qwe@qwe.com";

    // when
    final boolean existsByEmail = memberRepository.existsByEmail(newEmail);

    // then
    assertThat(existsByEmail).isFalse();
  }

}
