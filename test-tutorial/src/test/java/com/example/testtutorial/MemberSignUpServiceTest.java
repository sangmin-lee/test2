package com.example.testtutorial;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class MemberSignUpServiceTest {

  @InjectMocks
  private MemberSignUpService service;

  @Mock
  private MemberRepository repository;

  private Member member;

  @BeforeEach
  void setUp() {
    String email = "asd@asd.com";
    Name name = Name.builder()
        .first("a").middle("b").last("c").build();
    member = new Member(email, name);
  }

  @Test
  @DisplayName("회원가입 성공")
  void givenSignUpRequestWhenCallSignUpThenReturnSignUpMember() {
    // given
    final String email = member.getEmail();
    final Name name = member.getName();
    final SignUpRequest signUpRequest = new SignUpRequest(email, name);

    given(repository.existsByEmail(any())).willReturn(false);
    given(repository.save(any())).willReturn(member);

    // when
    final Member registeredMember = service.signUp(signUpRequest);

    // then
    assertThat(registeredMember)
        .isNotNull();
    assertThat(registeredMember.getEmail())
        .isEqualTo(member.getEmail());
    assertThat(registeredMember.getName().getFullName())
        .isEqualTo(member.getName().getFullName());
  }

  @Test
  @DisplayName("회원가입 실패")
  void givenSignUpRequestWhenCallSignUpThenThrowsException() {
    // given
    final String email = member.getEmail();
    final Name name = member.getName();
    final SignUpRequest signUpRequest = new SignUpRequest(email, name);

    given(repository.existsByEmail(any())).willReturn(true);

    // then
    assertThrows(EmailAlreadyExistsExceptions.class,
        () -> service.signUp(signUpRequest));
  }

}