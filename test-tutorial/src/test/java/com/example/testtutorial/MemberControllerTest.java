package com.example.testtutorial;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class MemberControllerTest {

  @Autowired
  protected MockMvc mvc;
  @Autowired
  protected ObjectMapper objectMapper;
  @Autowired
  private MemberSignUpService signUpService;

  @Test
  @DisplayName("회원가입 성공")
  void givenSignUpRequestWhenSignUpThenReturnOkAndId() throws Exception {
    // given
    final String email = "asd@asd.com";
    final Name name = Name.builder()
        .first("a").middle("b").last("c").build();
    final SignUpRequest signUpRequest = new SignUpRequest(email, name);

    // when
    ResultActions resultActions = getResultActions(signUpRequest);

    // then
    resultActions
        .andExpect(status().isOk())
        .andExpect(jsonPath("email").value(email));
  }

  @Test
  @DisplayName("회원가입 실패")
  void givenSignUpRequestWhenSignUpThenReturnConflict() throws Exception {
    // given
    final String email = "asd@asd.com";
    final Name name = Name.builder()
        .first("a").middle("b").last("c").build();
    SignUpRequest signUpRequest = new SignUpRequest(email, name);

    signUpService.signUp(signUpRequest);

    // when
    ResultActions resultActions = getResultActions(signUpRequest);

    // then
    resultActions.andExpect(status().isConflict());
  }

  private ResultActions getResultActions(SignUpRequest signUpRequest) throws Exception {
    return mvc.perform(post("/members")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .content(objectMapper.writeValueAsString(signUpRequest)))
        .andDo(print());
  }

}