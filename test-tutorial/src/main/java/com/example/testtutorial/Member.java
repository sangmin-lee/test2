package com.example.testtutorial;

import lombok.Getter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
public class Member {

  @Id
  @GeneratedValue
  private Long id;

  private String email;

  @Embedded
  private Name name;

  public Member(String email, Name name) {
    this.email = email;
    this.name = name;
  }
}
