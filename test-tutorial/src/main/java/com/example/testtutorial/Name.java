package com.example.testtutorial;

import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import static org.springframework.util.StringUtils.hasLength;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString(of = {"first", "middle", "last"})
public class Name {

  @Column(name = "first_name", length = 50)
  private String first;

  @Column(name = "middle_name", length = 50)
  private String middle;

  @Column(name = "last_name", length = 50)
  private String last;

  @Builder
  public Name(final String first, final String middle, final String last) {
    this.first = first;
    this.middle = hasLength(middle) ? middle : null;
    this.last = last;
  }

  public String getFullName() {
    if (this.middle == null) {
      return String.format("%s %s", this.first, this.last);
    }
    return String.format("%s %s %s", this.first, this.middle, this.last);
  }

}
