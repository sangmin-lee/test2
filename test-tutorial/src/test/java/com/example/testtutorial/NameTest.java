package com.example.testtutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NameTest {

  @Test
  @DisplayName("getFullName 메서드 테스트")
  void givenFullNameWhenGetFullNameThenReturnFullName() {
    // given
    String first = "first", middle = "middle", last = "last";

    final Name name = Name.builder()
        .first(first)
        .middle(middle)
        .last(last)
        .build();

    // when
    final String fullName = name.getFullName();

    // then
    assertThat(fullName).isEqualTo(first + " " + middle + " " + last);
  }

  @Test
  @DisplayName("middle 없이 Name을 생성한 뒤 getFullName 메서드 테스트")
  void givenFirstNameAndLastNameWhenGetFullNameThenReturnName() {
    // given
    String first = "first", last = "last";
    final Name name = Name.builder()
        .first(first)
        .last(last)
        .build();

    // when
    final String fullName = name.getFullName();

    // then
    assertThat(fullName).isEqualTo(first + " " + last);
    assertThat(name.getMiddle()).isNull();
  }

}
