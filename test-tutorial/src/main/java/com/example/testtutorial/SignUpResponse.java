package com.example.testtutorial;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpResponse {

  private String email;

  public SignUpResponse(Member member) {
    this.email = member.getEmail();
  }

}
